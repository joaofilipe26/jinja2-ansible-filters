## [1.2.1](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/compare/v1.2.0...v1.2.1) (2019-11-15)


### Bug Fixes

* Fixed description by removing quotes ([2d29bff](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/2d29bff))

# [1.2.0](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/compare/v1.1.0...v1.2.0) (2019-11-07)


### Features

* removed six requirement from setup.cfg ([4902da9](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/4902da9))

# [1.1.0](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/compare/v1.0.0...v1.1.0) (2019-11-06)


### Features

* Added test  coverage to satisfactory level ([f14ca2c](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/f14ca2c))
* Initial commit with full function and tests ([702e321](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/702e321))

# 1.0.0 (2019-11-06)


### Features

* Added test  coverage to satisfactory level ([bbb0cb0](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/bbb0cb0))
* Initial commit with full function and tests ([529c606](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/529c606))
